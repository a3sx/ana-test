

module "weby" {
  source = "./modules/weby"

  container_port  = "80"
  cpu_target      = 90
  memory_target   = 90
  max_capacity    = 4
  min_capacity    = 1
  certificate_arn = "FILLMEIN"
}
data "aws_subnet" "web_a" {
  vpc_id            = module.web_eu_west_1_vpc.vpc_id
  availability_zone = "eu-west-1a"
  filter {
    name   = "tag:Name"
    values = ["web-private"]
  }
}

data "aws_subnet" "web_b" {
  vpc_id            = module.web_eu_west_1_vpc.vpc_id
  availability_zone = "eu-west-1b"
  filter {
    name   = "tag:Name"
    values = ["web-private"]
  }
}

data "aws_subnet" "web_c" {
  vpc_id            = module.web_eu_west_1_vpc.vpc_id
  availability_zone = "eu-west-1c"
  filter {
    name   = "tag:Name"
    values = ["web-private"]
  }
}

module "web_eu_west_1_vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> v3.6.0"

  name                            = "web"
  cidr                            = var.web_eu_west_1_cidr
  azs                             = var.web_eu_west_1_azs
  private_subnets                 = var.web_private_eu_west_1_subnets
  public_subnets                  = var.web_public_eu_west_1_subnets
  map_public_ip_on_launch         = false
  enable_nat_gateway              = true
  single_nat_gateway              = false
  one_nat_gateway_per_az          = true
  enable_dns_support              = true
  enable_dns_hostnames            = true
  enable_ipv6                     = true
  assign_ipv6_address_on_creation = false

  tags = {
    Name = "web"
  }
  public_subnet_tags = {
    Name = "web-public"
  }
  public_route_table_tags = {
    Name = "web-public"
  }
  public_acl_tags = {
    Name = "web-public"
  }
  private_route_table_tags = {
    Name = "web-private"
  }
  private_subnet_tags = {
    Name = "web-private"
  }
  private_acl_tags = {
    Name = "web-private"
  }
}


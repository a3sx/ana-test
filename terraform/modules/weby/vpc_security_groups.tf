resource "aws_security_group" "ecs_task" {
  vpc_id = module.web_eu_west_1_vpc.vpc_id
  tags = {
    Name = "ecs-task"
  }
}

resource "aws_security_group_rule" "ingress_https" {
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.ecs_task.id
  to_port           = 443
  cidr_blocks       = var.nlb_ips
  type              = "ingress"
}

resource "aws_security_group_rule" "egress_all" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.ecs_task.id
  to_port           = 0
  cidr_blocks       = var.egress
  type              = "egress"
}
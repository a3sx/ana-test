variable "nlb_ips" {
  type    = list(string)
  default = ["172.18.128.20", "172.18.128.80"]
}

variable "egress" {
  type    = list(string)
  default = ["0.0.0.0/0"]
}

variable "container_port" {
  type    = string
  default = "80"
}

variable "cpu_target" {
  type    = number
  default = 90
}

variable "memory_target" {
  type    = number
  default = 90
}

variable "max_capacity" {
  type    = number
  default = 4
}

variable "min_capacity" {
  type    = number
  default = 1
}

variable "web_eu_west_1_cidr" {
  type    = string
  default = "172.18.128.0/24"
}

variable "web_eu_west_1_azs" {
  type = list(string)
  default = [
    "eu-west-1a",
    "eu-west-1b",
    "eu-west-1c"
  ]
}

variable "web_private_eu_west_1_subnets" {
  type = list(string)
  default = [
    "172.18.128.0/26",
    "172.18.128.64/27",
    "172.18.128.96/27"
  ]
}

variable "web_public_eu_west_1_subnets" {
  type = list(string)
  default = [
    "172.18.128.128/26",
    "172.18.128.192/27",
    "172.18.128.224/27"
  ]
}

variable "certificate_arn" {
  type = string
}

resource "aws_ecs_service" "web_tcp" {
  name            = "web-tcp"
  cluster         = aws_ecs_cluster.web.id
  task_definition = aws_ecs_task_definition.web_tcp.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    subnets         = [data.aws_subnet.web_a.id]
    security_groups = [aws_security_group.ecs_task.id]
  }
  lifecycle {
    ignore_changes = [
    desired_count]
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.https_tcp.arn
    container_name   = "web"
    container_port   = var.container_port
  }

  depends_on = [aws_lb.https_tls]
}
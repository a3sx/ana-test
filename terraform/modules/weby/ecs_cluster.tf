resource "aws_ecs_cluster" "web" {
  name = "web"
  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_cluster_capacity_providers" "web" {
  cluster_name       = aws_ecs_cluster.web.name
  capacity_providers = ["FARGATE"]
}

resource "aws_ecs_task_definition" "web_tcp" {
  family                   = "web-tcp"
  container_definitions    = file("${path.module}/service.json")
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 2048
  memory                   = 4096
  execution_role_arn       = aws_iam_role.ecs_service.arn
  task_role_arn            = aws_iam_role.ecs_service.arn
}
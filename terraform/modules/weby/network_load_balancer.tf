resource "aws_lb" "https_tls" {
  name                             = "https-tls"
  internal                         = true
  load_balancer_type               = "network"
  enable_deletion_protection       = true
  enable_cross_zone_load_balancing = true
  ip_address_type                  = "ipv4"
  subnet_mapping {
    subnet_id            = data.aws_subnet.web_a.id
    private_ipv4_address = var.nlb_ips[0]
  }
  subnet_mapping {
    subnet_id            = data.aws_subnet.web_b.id
    private_ipv4_address = var.nlb_ips[1]
  }
}

resource "aws_lb_target_group" "https_tcp" {
  name               = "https-tcp"
  port               = 443
  protocol           = "TCP"
  vpc_id             = module.web_eu_west_1_vpc.vpc_id
  target_type        = "ip"
  preserve_client_ip = true
  health_check {
    enabled             = true
    port                = "443"
    protocol            = "TCP"
    interval            = 10
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }
}

resource "aws_lb_listener" "https_443_tcp" {
  load_balancer_arn = aws_lb.https_tls.arn
  port              = "443"
  protocol          = "TLS"
  certificate_arn   = var.certificate_arn
  ssl_policy        = "ELBSecurityPolicy-TLS13-1-3-2021-06"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.https_tcp.arn
  }
}

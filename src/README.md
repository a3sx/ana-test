## Build

```
 docker build -t web:v1 .
```

## Run
```
 docker run -d -p 8080:80 web:v1 
```

visit http://localhost:8080
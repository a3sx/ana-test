# Part 1

This is a teraform module that deploys a simple web app onto AWS ECS whilst taking advantage of autoscalling to meet demand based on CPU/Memory usage of the container. A VPC is deployed along
side it with multi AZ redundancy. There is a load balancer infront of the ECS app which takes advantage of AWS certs and allows us to use https for the app. ECS will remove unhealthy containers based on the 443 health check.

Install terragrunt alongside terraform https://terragrunt.gruntwork.io/docs/getting-started/install/ 

Export AWS creds on the cli

## Deploy
Use terragrunt instead of terraform command to take advantage of terragrunt.hcl

```
cd terraform
terragrunt init
terragrunt plan
terragrunt apply
```

## Test 

The Gitlab CI will run some basic formatting and terraform validation against the terraform code.

It also builds the web app docker image to test everything runs there

# Part 2

Ideally i wouldnt add a way to provide shell access to the containers as this is against best
security practices. If i had to provide some shell access i would take 
advantage of the ECS Exec feature built into AWS ECS. ECS Exec uses AWS SSM
to create a secure session for a user who has been granted the correct AIM access to do so.


All the logs from the containers are sent to AWS Cloudwatch via the ECS log driver built into
the ECS service. Logs related to the shell access would be logged via the AWS SSM manager.

